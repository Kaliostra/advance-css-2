const hbm = document.querySelector(".header__menu");
const hb = document.querySelector(".header__burger");
const links = document.querySelectorAll(".header__menu a");

hb.addEventListener("click", function (event) {
  hbm.classList.toggle("active-menu");
  hb.classList.toggle("active-burger");
  event.stopPropagation();
});

links.forEach((link) => {
  link.addEventListener("click", function (event) {
    event.preventDefault();
  });
});

document.body.addEventListener("click", function (event) {
  if (event.target !== hbm) {
    if (hbm.classList.contains("active-menu")) {
      hbm.classList.remove("active-menu");
    }
    if (hb.classList.contains("active-burger")) {
      hb.classList.remove("active-burger");
    }
  }
});
